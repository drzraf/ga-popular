<?php

/*
 * Copyright (c) 2021 Raphaël . Droz + floss @ gmail DOT com
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version
 *
 * Fetch popular posts from Google Analytics.
 */

namespace GaPopular;

use \Google\Analytics\Data\V1beta as GAv4;

class FetchCommandGAv4 extends FetchCommand
{
    public function __invoke($args, $assoc_args)
    {
        parent::invoke($args, $assoc_args);
    }

    public static function get_cli_parameters()
    {
        $parameters = parent::COMMAND_ARGS;

        array_unshift($parameters, [
            'type' => 'positional',
            'name' => 'property_id',
            'optional' => false,
            'repeating' => false
        ]);

        $parameters = array_map(function($parameter) {
            if ($parameter['name'] === 'metrics') {
                $parameter['description'] = 'See https://developers.google.com/analytics/devguides/reporting/data/v1/api-schema#metrics';
                $parameter['default'] = 'screenPageViews';
            }

            if ($parameter['name'] === 'start-date') {
                $parameter['description'] = 'The start date. See https://developers.google.com/analytics/devguides/reporting/data/v1/rest/v1beta/DateRange';
            }

            if ($parameter['name'] === 'regex') {
                $parameter['description'] = 'See https://developers.google.com/analytics/devguides/reporting/data/v1/rest/v1beta/FilterExpression';
            }

            return $parameter;
        }, $parameters);
        return $parameters;
    }

    public static function get_report($property_id, $start_date, $service_account_file, $opts)
    {
        try {
            $config = [
                'property' => 'properties/' . $property_id,
                'dateRanges' => [
                    new GAv4\DateRange([
                        'start_date' => $start_date,
                        'end_date' => 'today',
                    ]),
                ],
                'dimensions' => [
                    new GAv4\Dimension(['name' => 'pagePath'])
                ],
                'metrics' => array_map(function ($metric) {
                    return new GAv4\Metric(['name' => $metric]);
                }, $opts['metrics']),
                'orderBys' => [
                    new GAv4\OrderBy([
                        'desc' => true,
                        'metric' => new GAv4\OrderBy\MetricOrderBy([
                            'metric_name' => $opts['metrics'][0]
                        ])
                    ])
                ],
                'limit' => $opts['limit'] ?: 10
            ];

            $client = new GAv4\BetaAnalyticsDataClient([
                'credentials' => \Google\ApiCore\CredentialsWrapper::build([
                    'scopes'  => [
                        'https://www.googleapis.com/auth/analytics',
                        'openid',
                        'https://www.googleapis.com/auth/analytics.readonly',
                    ],
                    'keyFile' => self::get_credentials($service_account_file),
                ]),
            ]);

            if ($opts['regex']) {
                $is_negative = substr($opts['regex'], 0, 1) == '!';
                $opts['regex'] = str_replace('/', '\/', $opts['regex']);
                $dimensionFilterExpression = new GAv4\FilterExpression();

                $filter = new GAv4\Filter([
                    'field_name' => 'pagePath',
                    'string_filter' => new GAv4\Filter\StringFilter([
                        'value' => $is_negative ? substr($opts['regex'], 1) : $opts['regex'],
                        'match_type' => GAv4\Filter\StringFilter\MatchType::FULL_REGEXP
                    ])
                ]);

                if ($is_negative) {
                    $dimensionFilterExpression->setNotExpression(new GAv4\FilterExpression([
                        'filter' => $filter
                    ]));
                } else {
                    $dimensionFilterExpression->setFilter($filter);
                }

                $config['dimensionFilter'] = $dimensionFilterExpression;
            }

            $response = $client->runReport($config);
            $results = [];

            foreach ($response->getRows() as $row) {
                $results[] = $row->getDimensionValues()[0]->getValue();
            }

            return $results;
        } catch (\Throwable $th) {
            echo $th;
        }
    }
}
