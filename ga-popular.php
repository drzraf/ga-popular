<?php

/**
 * Plugin Name: ga-popular
 * Plugin URI: https://gitlab.com/drzraf/ga-popular
 * Description: Fetch popular posts IDs from Google Analytics.
 * Version: 0.1.0
 * Author: Raphaël Droz
 * Author URI: https://gitlab.com/drzraf
 * License: GPL-3.0
 */

/*
 * Copyright (c) 2021 Raphaël . Droz + floss @ gmail DOT com
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version
 */

namespace GaPopular;

if (defined('WP_CLI')) {
    if (file_exists(__DIR__ . '/vendor/autoload.php')) {
        require_once __DIR__ . '/vendor/autoload.php';
    }

    add_action('init', function () {
        \WP_CLI::add_command('ga-popular fetch', new FetchCommandUniversalAnalytics(), ['synopsis' => FetchCommandUniversalAnalytics::get_cli_parameters()]);
        \WP_CLI::add_command('ga-popular fetchv4', new FetchCommandGAv4(), ['synopsis' => FetchCommandGAv4::get_cli_parameters()]);
    });
}
