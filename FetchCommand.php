<?php

/*
 * Copyright (c) 2021 Raphaël . Droz + floss @ gmail DOT com
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version
 *
 * Fetch popular posts from Google Analytics.
 */

namespace GaPopular;

use Exception;

abstract class FetchCommand
{
    const KNOWN_OPTS = [
        'metrics',
        'start-date',
        'query-string',
        'regex',
        'limit',
        'db-store',
        'json-config',
        'output-format',
        'output-file',
        'credentials-file',
    ];

    const COMMAND_ARGS = [
        [
            'type' => 'assoc',
            'name' => 'metrics',
            'optional' => false,
            'repeating' => false
        ],
        [
            'type' => 'assoc',
            'name' => 'start-date',
            'optional' => false,
            'repeating' => false,
            'default' => '180daysAgo'
        ],
        [
            'type' => 'assoc',
            'name' => 'limit',
            'description' => 'Limit to that many results',
            'optional' => false,
            'repeating' => false,
            'default' => 10
        ],
        [
            'type' => 'assoc',
            'name' => 'regex',
            'optional' => true,
            'repeating' => false
        ],
        [
            'type' => 'assoc',
            'name' => 'json-config',
            'desciption' => 'JSON configuration file',
            'optional' => true,
            'repeating' => false
        ],
        [
            'type' => 'assoc',
            'name' => 'credentials-file',
            'desciption' => 'JSON oAuth credentials file',
            'optional' => false,
            'repeating' => false,
            'default' => 'service_account.json'
        ],
        [
            'type' => 'assoc',
            'name' => 'output-format',
            'description' => 'Output format options (comma-separated). Possible values: "url", "ids", "meta", "metrics"
                    - "url" (default) Dump URLS.
                    - "meta" Add meta data about current command-line run to the JSON output.
                    - "metrics": Additionally provide metrics values.
                    - "wp-id": Provide WP post IDs. If both "url" and "wp-id" are set, the results are formed
                ',
            'optional' => false,
            'repeating' => false,
            'default' => 'url',
            'options' => array('url', 'meta', 'metrics', 'wp-id'),
        ],
        [
            'type' => 'assoc',
            'name' => 'db-store',
            'desciption' => 'Set the values as an ordered list of post IDs as an option named "ga_popular_ids_<value>".',
            'optional' => true,
            'repeating' => false,
            'default' => 'default',
            'value' => [
              'optional' => true
            ],
        ],
        [
            'type' => 'assoc',
            'name' => 'output-file',
            'desciption' => 'Output file',
            'optional' => false,
            'repeating' => false,
            'default' => 'php://stdout'
        ],
    ];

    public function invoke($args, $assoc_args)
    {
        $cfg = [];
        if (!empty($assoc_args['json-config'])) {
            try {
                $cfg = json_decode(file_get_contents($assoc_args['json-config']), true);
            } catch (Exception $f) {
            }
        }

        if (($assoc_args['db-store'] ?? false) === true) {
            $assoc_args['db-store'] = 'default';
        }

        if (is_string($assoc_args['db-store'] ?? false)) {
            $assoc_args['db-store'] = preg_replace('/[^a-z0-9_]/i', '', $assoc_args['db-store']);
        }

        $getopt = function ($opt) use ($cfg, $assoc_args) {
            global $argv;
            $on_cmdline = strpos(implode(' ', $argv), '--' . $opt) !== false;
            return $on_cmdline && array_key_exists($opt, $assoc_args) ? $assoc_args[$opt] : ($cfg[$opt] ?? $assoc_args[$opt] ?? false);
        };

        $opts = array_combine(
            self::KNOWN_OPTS,
            array_map(function ($e) use ($getopt) {
                return $getopt($e);
            }, self::KNOWN_OPTS)
        );

        $view_id = !empty($cfg['view_id']) ? $cfg['view_id'] : ($cfg['property_id'] ?? $args[0]);
        $results = static::fetch($view_id, $opts);
        $results = self::add_meta($results, $view_id, $opts);
        self::enrich_data($results, $opts);

        self::store($results, $opts);
    }

    // https://github.com/googleapis/google-api-php-client/issues/2136#issuecomment-952894653
    public static function auth($tokenPath, $client)
    {
        if (file_exists($tokenPath)) {
            $accessToken = json_decode(file_get_contents($tokenPath), true);
            $client->setAccessToken($accessToken);
        }

        // If there is no previous token or it's expired.
        if (!$client->isUsingApplicationDefaultCredentials() && $client->isAccessTokenExpired()) {
            // Refresh the token if possible, else fetch a new one.
            if ($client->getRefreshToken()) {
                $client->fetchAccessTokenWithRefreshToken($client->getRefreshToken());
            } else {
                // Request authorization from the user.
                $authUrl = $client->createAuthUrl();
                fprintf(STDOUT, "Open the following link in your browser:\n%s\n", $authUrl);
                fprintf(STDOUT, 'Enter verification code: ');
                $authCode = trim(fgets(STDIN));

                // Exchange authorization code for an access token.
                $accessToken = $client->fetchAccessTokenWithAuthCode($authCode);
                $client->setAccessToken($accessToken);

                // Check to see if there was an error.
                if (array_key_exists('error', $accessToken)) {
                    throw new Exception(join(', ', $accessToken));
                }
            }

            // Save the token to a file.
            if (!file_exists(dirname($tokenPath))) {
                mkdir(dirname($tokenPath), 0700, true);
            }

            file_put_contents($tokenPath, json_encode($client->getAccessToken()));
        }

        return $client;
    }

    public static function get_credentials($service_account_file)
    {
        return strpos($service_account_file, '/dev/fd/') === 0 || strpos($service_account_file, '/proc/self/fd/') === 0
            ? json_decode(file_get_contents('php://' . str_replace(['/dev/', '/proc/self/'], ['', ''], $service_account_file)), true)
            : ($service_account_file === '-' ? json_decode(stream_get_contents(STDIN), true) : $service_account_file);
    }

    public static function enrich_data(&$results, $opts)
    {
        $as_ids = in_array('wp-id', explode(',', $opts['output-format']));
        $as_url = in_array('url', explode(',', $opts['output-format']));
        $db_store = $opts['db-store'] ?? false;

        if (!$as_ids && !$db_store) {
            return;
        }

        if (isset($results[0])) {
            $results = array_map(function ($e) use ($as_url) {
                return $as_url ? [$e, url_to_postid($e)] : url_to_postid($e);
            }, array_values($results));
        } elseif (isset($results['data']) && $as_ids) {
            foreach ($results['data'] as $k => &$v) {
                if (is_array($v)) {
                    $v['id'] = url_to_postid($k);
                }
            }
        } elseif ($as_ids) {
            foreach ($results as $url => &$v) {
                if (is_array($v)) {
                    $v['id'] = url_to_postid($url);
                }
            }
        }

        if ($db_store) {
            unset($results['credentials-file']);
            $option_name = 'ga_popular_ids_' . (is_string($db_store) ? $db_store : 'default');
            return update_option($option_name, $results);
        }
    }

    public static function fetch($view_id, $opts)
    {
        if (is_string($opts['metrics'])) {
            $opts['metrics'] = explode(',', $opts['metrics']);
        }

        return static::get_report($view_id, $opts['start-date'], $opts['credentials-file'], $opts);
    }

    public static function add_meta($results, $view_id, $opts)
    {
        $with_meta = in_array('meta', explode(',', $opts['output-format']));
        return !$with_meta
            ? $results
            : [
                'view_id' => $view_id,
                'date' => time(),
                'data' => $results
            ] + $opts;
    }

    public static function store($results, $opts)
    {
        file_put_contents($opts['output-file'], json_encode($results, JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES));
    }

    public static function run($view_id, $opts)
    {
        $results = static::fetch($view_id, $opts);
        $results = self::add_meta($results, $view_id, $opts);
        self::store($results, $opts);
        return $results;
    }

    abstract function __invoke($args, $assoc_args);
    abstract static function get_report($id, $start_date, $service_account_file, $opts);
    abstract static function get_cli_parameters();
}
