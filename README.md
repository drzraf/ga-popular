# Google Analytics popular posts.

Collect statistics from a given Google Analytics view.

This plugin wraps a WordPress wp-cli command and is intended to be run from the command-line or from a regularly called cron script.

It uses the Google Analytics API (and relies upon a credential file) to collect the statistics from a specific Google Analytics view.

Viewing, filtering, sorting, ordering and limits, ... can be provided from command-line or from a JSON configuration file.

Output can be customized from a plain list of URL or a more extended JSON format containing desired metrics (pageViews, users, ...)

The command optionally allow to:
- determinate post ID corresponding to URL (In some WordPress usages)
- store the results as a WordPress option.

### Benefits
- A self-contained and simple plugin providing other themes/plugins Google Analytics data they can directly consume using `get_option()`.
- Protected Google Analytics credentials (Could be owned by root and piped to command's stdin. WordPress bootstrap is optional)
- A flexible way to customize statistics: A theme could write a valid JSON configuration file to be used.

### Examples
- `$ symfony-cli.php -c my-project-abcd.json -m ga:pageViews,ga:users -s 30daysAgo -r /recipes 181123123`
  Use said configuration and look for a `service_account.json` credential file to fetch URL in `/recipes` of view _181123123_ in the last month
  (command-line override a possibly different start-date specified by `my-project-abcd.json`)

- `$ wp ga-popular --credentials-file my-project-abcd.json --metrics ga:pageViews,ga:users --regex /show 181321123`
  Use the WP. Fetch said metrics for URL in `/show` for the last 6 months (default start date) for the view _181321123_ using a particular authentication file.

- `$ sudo cat /etc/ga-auth.json | wp @aeus ga-popular fetch --credentials-file=- --limit=6 --output-format=meta,ids --regex='!^/$' --db-store 176385704`
  Use a root-owned credential file passed as stdin and fetch up to 6 entries not including the homepage storing the result in the `ga_popular_ids_default` WP option.

### Output format

Any other format other than the plain list of URL will result in a dict whose keys are the URL and the data (metrics, post ID, ...) are a dict.
If `meta` is set, this result will additionally be wrapped under `data` (the other dict keys defining the configuration the script has been run with)

* `output-format=url` (default)
```json
[
    "/blog/2019/09/30/foo-bar",
    "/news/baz-blah/",
    "/blog/2019/07/26/nop-zob/"
]
```

* `output-format=metrics`
```json
{
    "/blog/2019/09/30/foo-bar": {
        "ga:pageViews": [
            "31724"
        ]
    },
    "/news/baz-blah/": {
        "ga:pageViews": [
            "23828"
        ]
    },
    "/blog/2019/07/26/nop-zob/": {
        "ga:pageViews": [
            "18997"
        ]
    },
    "data": null
}
```

* `output-format=meta`
```json
{
    "view_id": "176123132",
    "date": 1625591485,
    "data": [
        "/blog/2019/09/30/foo-bar",
        "/news/baz-blah/",
        "/blog/2019/07/26/nop-zob/"
    ],
    "metrics": "ga:pageViews",
    "start-date": "180daysAgo",
    "query-string": false,
    "regex": "!^/$",
    "limit": "3",
    "db-store": false,
    "json-config": false,
    "output-format": "meta",
    "output-file": "php://stdout",
    "credentials-file": "-"
}
```

* `output-format=wp-id` (Implies a dict-like structure and `metrics` presence)
```json
{
    "/blog/2019/09/30/foo-bar": {
        "ga:pageViews": [
            "31725"
        ],
        "id": 13196
    },
    "/news/baz-blah/": {
        "ga:pageViews": [
            "23829"
        ],
        "id": 4846
    },
    "/blog/2019/07/26/nop-zob/": {
        "ga:pageViews": [
            "18999"
        ],
        "id": 12617
    }
}
```

### References
* https://developers.google.com/analytics/devguides/reporting/core/v3/reference#startDate
* https://ga-dev-tools.appspot.com/dimensions-metrics-explorer/
