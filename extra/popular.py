#!/usr/bin/env python3

# Copyright (c) 2021 Raphaël . Droz + floss @ gmail DOT com
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version

import re
import sys
import time
import json
import argparse
import googleapiclient.discovery
from google.oauth2 import service_account
from google_auth_oauthlib.flow import InstalledAppFlow


SCOPES = ["https://www.googleapis.com/auth/analytics.readonly"]

def get_report(view_id, start_date, service_account_file, limit=10, metrics=["ga:pageViews"], regex=None):
    if service_account_file.name == '<stdin>':
        credentials = service_account.Credentials.from_service_account_info(
            json.load(service_account_file),
            scopes=SCOPES
        )
    else:
        is_openid = 'web' in json.load(service_account_file)
        if not is_openid:
            credentials = service_account.Credentials.from_service_account_file(
                service_account_file.name,
                scopes=SCOPES
            )
        else:
            flow = InstalledAppFlow.from_client_secrets_file(service_account_file.name, scopes=SCOPES)
            flow.run_local_server(port=9036)
            credentials = flow.credentials

    analytics = googleapiclient.discovery.build(
        serviceName="analyticsreporting", version="v4", credentials=credentials,
    )

    body = {
        "reportRequests": [
            {
                "viewId": view_id,
                "dateRanges": [{"startDate": start_date, "endDate": "today"}],
                "metrics": [{"expression": v} for v in metrics],
                "dimensions": [{"name": "ga:pagePath"}],
                "orderBys": [{"fieldName": metrics[0], "sortOrder": "DESCENDING"}],
                "pageSize": limit
            }
        ]
    }

    if regex:
        body['reportRequests'][0]['filtersExpression'] = "ga:pagePath=~" + regex

    return analytics.reports().batchGet(body=body).execute()

def get_detailled_popular_pages(response):
    results = {}
    reports = response.get("reports", [])
    if reports:
        report = reports[0]
        for row in report.get("data", {}).get("rows", []):
            results[row["dimensions"][0]] = row["metrics"][0]["values"]
    return results

def get_popular_pages(response):
    results = []
    reports = response.get("reports", [])
    if reports:
        report = reports[0]
        for row in report.get("data", {}).get("rows", []):
            results.append(row["dimensions"][0])
    return results

def main():
    parser = argparse.ArgumentParser(description="Provide poppular posts from Google Analytics",
                                     formatter_class=argparse.RawTextHelpFormatter,
                                     epilog='''
Example:
$ ga-popular.py -c my-project-abcd.json -m ga:pageViews,ga:users -s 30daysAgo -r /recipes 181415123
                                     ''')
    parser.add_argument('view_id', metavar='view-id', type=str, nargs=None, help='The GA view ID.')
    parser.add_argument('-m', '--metrics', type=str, nargs=None, help='Comma-separated list of metrics', default='ga:pageViews')
    parser.add_argument('-s', '--start-date', nargs=None, default='7daysAgo', help='The start date\n'
                        + 'See https://developers.google.com/analytics/devguides/reporting/core/v3/reference#startDate')

    parser.add_argument('-v', '--verbose', action='count', help='Verbose output', default=0)
    parser.add_argument('--info', action='store_true', help='Output metrics values too', default=False)
    parser.add_argument('-l', '--limit', nargs=None, help='Limit to that many results', default=10)
    parser.add_argument('-r', '--regex', nargs=None, help='An optional regexp to filter results')

    parser.add_argument('-c', '--credentials-file', nargs=None, type=argparse.FileType('r'), help='JSON oAuth credentials file', default="service_account.json")
    parser.add_argument('-o', '--output-file', nargs=None, type=argparse.FileType('w'), help='Output file', default=sys.stdout)
    args = parser.parse_args()

    metrics = args.metrics.split(',')
    response = get_report(args.view_id, args.start_date, args.credentials_file, args.limit, metrics, args.regex)
    pages = get_detailled_popular_pages(response) if args.verbose > 0 else get_popular_pages(response)

    if args.info:
        pages = {'start_date': args.start_date,
                 'regexp': args.regex,
                 'limit': args.limit,
                 'view_id': args.view_id,
                 'info': args.info,
                 'verbose': args.verbose,
                 'output-file': args.output_file.name,
                 'credentials-file': args.credentials_file.name,
                 'date': int(time.time()),
                 'data': pages}

    args.output_file.write(json.dumps(pages, indent=4))

if __name__ == "__main__":
    main()
