#!/usr/bin/php
<?php

/*
 * Copyright (c) 2021 Raphaël . Droz + floss @ gmail DOT com
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version
 */

namespace GaPopular;

require_once __DIR__ . '/vendor/autoload.php';

use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\SingleCommandApplication;
use GaPopular\FetchCommandUniversalAnalytics;

(new SingleCommandApplication())
    ->setName('Google Analytics popular posts')
    ->setVersion('0.0.1')
    ->setDescription('Provide poppular posts from Google Analytics')
    ->setHelp(<<<EOF
              Example:
$ ga-popular.py -c my-project-abcd.json -m ga:pageViews,ga:users -s 30daysAgo -r /recipes 181415123
EOF
    )
    ->addArgument('view_id', InputArgument::REQUIRED, 'Google Analytics view ID.')
    ->addOption('metrics', 'm', InputOption::VALUE_REQUIRED, 'Comma-separated list of metrics' . PHP_EOL .
                'See https://ga-dev-tools.appspot.com/dimensions-metrics-explorer/', 'ga:pageViews')
    ->addOption('start-date', 's', InputOption::VALUE_REQUIRED, 'The start date' . PHP_EOL .
                'See https://developers.google.com/analytics/devguides/reporting/core/v3/reference#startDate', '180daysAgo')
    ->addOption('limit', 'l', InputOption::VALUE_REQUIRED, 'Limit to that many results', 10)
    ->addOption('query-string', 'Q', InputOption::VALUE_NONE, 'Do not include URL containing query-string.' . PHP_EOL .
                'Shortcut for --regex=ga:pagePath!@?')
    ->addOption('regex', 'r', InputOption::VALUE_REQUIRED, 'An optional regexp to filter results' . PHP_EOL .
                'To express a match inversion, prefix by "!"' . PHP_EOL .
                'Eg: To exclude the home page use --regex="!^/$"' . PHP_EOL .
                'See https://developers.google.com/analytics/devguides/reporting/core/v3/reference#filters')
    ->addOption('json-config', 'j', InputOption::VALUE_REQUIRED, 'JSON configuration file')
    ->addOption('credentials-file', 'c', InputOption::VALUE_REQUIRED, 'JSON oAuth credentials file', 'service_account.json')
    ->addOption('output-format', 'f', InputOption::VALUE_OPTIONAL, <<<EOF
Output format options (comma-separated). Possible values: "url", "ids", "meta", "values"
- "url" (default) Dump URLS
- "meta" Add meta data about current command-line run to the JSON output.
- "metrics": Additionally provide metrics' values.
EOF
    )
    ->addOption('output-file', 'o', InputOption::VALUE_REQUIRED, 'Output file', 'php://stdout')
    ->setCode(function (InputInterface $input, OutputInterface $output) {
        $cfg = [];
        if ($input->getOption('json-config')) {
            try {
                $cfg = json_decode(file_get_contents($input->getOption('json-config')), true);
            } catch (Exception $f) {
            }
        }

        $getopt = function ($opt) use ($cfg, $input) {
            $on_cmdline = $input->hasParameterOption('--' . $opt) || $input->hasParameterOption('-' . $opt[0]);
            return $on_cmdline ? $input->getOption($opt) : ($cfg[$opt] ?? $input->getOption($opt));
        };

        $known_opts = [
          'metrics',
          'start-date',
          'query-string',
          'regex',
          'limit',
          'json-config',
          'output-format',
          'output-file',
          'credentials-file',
        ];

        $opts = array_combine(
            $known_opts,
            array_map(function ($e) use ($getopt) {
                return $getopt($e);
            }, $known_opts)
        );

        $view_id = $cfg['view_id'] ?? $input->getArgument('view_id');
        FetchCommandUniversalAnalytics::run($view_id, $opts);
    })
    ->run();
