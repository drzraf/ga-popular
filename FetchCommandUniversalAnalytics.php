<?php

/*
 * Copyright (c) 2021 Raphaël . Droz + floss @ gmail DOT com
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version
 *
 * Fetch popular posts from Google Analytics.
 */

namespace GaPopular;

class FetchCommandUniversalAnalytics extends FetchCommand
{
    public function __invoke($args, $assoc_args)
    {
        parent::invoke($args, $assoc_args);
    }

    public static function get_cli_parameters()
    {
        $parameters = parent::COMMAND_ARGS;

        array_unshift($parameters, [
            'type' => 'positional',
            'name' => 'view_id',
            'optional' => false,
            'repeating' => false
        ]);

        array_unshift($parameters, [
            'type' => 'assoc',
            'name' => 'query-string',
            'description' => 'Shortcut for --regex=ga:pagePath!@?',
            'repeating' => false,
            'default' => 'false'
        ]);

        $parameters = array_map(function($parameter) {
            if ($parameter['name'] === 'metrics') {
                $parameter['description'] = 'See https://ga-dev-tools.appspot.com/dimensions-metrics-explorer/';
                $parameter['default'] = 'ga:pageViews';
            }

            if ($parameter['name'] === 'start-date') {
                $parameter['description'] = 'The start date. See https://developers.google.com/analytics/devguides/reporting/core/v3/reference#startDate';
            }

            if ($parameter['name'] === 'regex') {
                $parameter['description'] = 'See https://developers.google.com/analytics/devguides/reporting/core/v3/reference#filters';
            }

            return $parameter;
        }, $parameters);
        return $parameters;
    }

    public static function get_report($view_id, $start_date, $service_account_file, $opts)
    {
        [
            'limit' => $limit,
            'metrics' => $metrics,
            'regex' => $regex,
            'query-string' => $query_string
        ] = $opts + ['limit' => 10, 'no_query_string' => false, 'regex' => null, 'metrics' => ['ga:pageViews']];

        $asObject = count(array_intersect(['metrics', 'wp-id'], explode(',', $opts['output-format'])));
        $client = new \Google_Client();
        $client->setAuthConfig(self::get_credentials($service_account_file));

        $client->addScope(\Google_Service_Analytics::ANALYTICS_READONLY);
        $client->setRedirectUri('http://localhost:9036/');
        $client->setAccessType('offline');
        $client->setPrompt('select_account consent');

        self::auth('token-cache.json', $client);

        // Configure the request
        $dateRange = new \Google_Service_AnalyticsReporting_DateRange();
        $dateRange->setStartDate($start_date);
        $dateRange->setEndDate('today');

        // Create the Metrics object.
        $gmetrics = [];
        foreach ($metrics as $v) {
            $x = new \Google_Service_AnalyticsReporting_Metric();
            $x->setExpression($v);
            $gmetrics[] = $x;
        }

        //Create the Dimensions object.
        $dimension = new \Google_Service_AnalyticsReporting_Dimension();
        $dimension->setName('ga:pagePath');

        $orderby = new \Google_Service_AnalyticsReporting_OrderBy();
        $orderby->setFieldName($metrics[0]);
        $orderby->setSortOrder('DESCENDING');

        // Create the ReportRequest object.
        $request = new \Google_Service_AnalyticsReporting_ReportRequest();
        $request->setViewId($view_id);
        $request->setDateRanges($dateRange);
        $request->setDimensions([$dimension]);
        $request->setMetrics($gmetrics);
        $request->setOrderBys([$orderby]);
        $request->setPageSize($limit);

        $filter = [];
        if (!$query_string) {
            $filter[] = 'ga:pagePath!@?';
        }
        if ($regex) {
            $operator = '=~';
            if (substr($regex, 0, 1) == '!') {
                $operator = '!~';
                $regex = substr($regex, 1);
            }

            $filter[] = 'ga:pagePath' . $operator . $regex;
        }

        if (count($filter)) {
            $request->setFiltersExpression(implode(';', $filter));
        }

        $body = new \Google_Service_AnalyticsReporting_GetReportsRequest();
        $body->setReportRequests([$request]);

        $analytics = new \Google_Service_AnalyticsReporting($client);

        return self::get_popular_pages($analytics->reports->batchGet($body)->getReports(), $asObject);
    }

    public static function get_popular_pages($reports, $asObject)
    {
        $results = [];
        if ($reports) {
            $headers = $reports[0]->getColumnHeader()->getMetricHeader()->getMetricHeaderEntries();
            $rows = $reports[0]->getData()->getRows() ?: [];
            foreach ($rows as $row) {
                if ($asObject) {
                    $values = [];
                    foreach ($row->getMetrics() as $k => $v) {
                        $values[$headers[$k]->getName()] = $v->getValues();
                    }
                    $results[$row->dimensions[0]] = $values;
                } else {
                    $results[] = $row->dimensions[0];
                }
            }
        }

        return $results;
    }
}
